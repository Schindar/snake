const gameState = {

    width: 8,
    height: 8,
    snake: [

        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 1, y: 3 },
        { x: 2, y: 3 },
        { x: 3, y: 3 }
    ],
    apple: { x: 5, y: 3 },
    direction: 'east'
    , stop: 1000
    , Over: false
};
// Your code from previous exercise here
let frameCount = 0;
let fps, frameTime, startTime, now, then;

//////////////////////////////////
function startAnimating(targetFPS) {
    frameTime = 1000 / targetFPS;
    then = window.performance.now();
    startTime = then;
    frameCount = 0;

    animate(then);

}


/////////////////////////////// 
function animate(currentTime) {

    if (frameCount <= gameState.stop ) {
        window.requestAnimationFrame(animate);
    }
    
    
    now = currentTime;
    const elapsed = now - then;

    if (elapsed > frameTime ) {

        then = now;
        console.log(currentTime + ': Rendering frame #' + frameCount);
        
        frameCount += 1;
        
        
        
       
        update()
        if(!gameState.Over){
            render(gameState,'myCanvas')

        }


    }

}



startAnimating(4);






////////////////////////////////

function update() {

    move_body_of_snake(gameState)
   
    
    console.log("(x:" + headx() + "-----_y:" + heady() + ")")

    switch (gameState.direction) {
        case 'east':
            moving_theHead_to_specific_direction(1, 0)
            break;
        case 'west':
            moving_theHead_to_specific_direction(-1, 0)
            break;
        case 'north':
            moving_theHead_to_specific_direction(0, -1)
            break;
        case 'south':
            moving_theHead_to_specific_direction(0, 1)
    }
    if (check_Contact_with_apple(gameState)) {
        find_free_place_for_food()


        gameState.snake.push({ x: gameState.apple.x, y: gameState.apple.y })

    } if (gameover(gameState) || collision(gameState)) {

        gameState.Over= true
        
        gameState.stop  = 0
        
    }


}
document.addEventListener('keyup', event => {
    if (event.key === 'ArrowRight') {

        gameState.direction = "east"

    } else if (event.key === 'ArrowLeft') {
        gameState.direction = "west"

    } else if (event.key === 'ArrowUp') {
        gameState.direction = "north"

    } else if (event.key === 'ArrowDown') {
        gameState.direction = "south"

    }

    console.log('Key ' + event.key + ' was pressed.');

});
//////////////////////// rendering
function render(canvasId) {

    const canvas = document.getElementById('myCanvas');
    const context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width, canvas.height);

    const h = gameState.height;
    const w = gameState.width;

    for (let i = 0; i <= w; i++) {
        for (let j = 0; j <= h; j++) {
            // drawing the grid

            drawLine(i * canvas.height / w, 0, i * canvas.height / w, canvas.height, context);
            drawLine(0, i * canvas.height / h, canvas.width, i * canvas.height / h, context);
            // checking the seqment position
            if (segment(gameState, i, j)) {

                drawing_squares(context, convert(i), convert(j), canvas.width / w - 1.5, canvas.height / h - 1.5, 'black')
            }
            // checking the apple position
            if (apple(gameState, i, j)) {


                drawing_squares(context, convert(i), convert(j), canvas.width / w - 1.5, canvas.height / h - 1.5, 'red')
            }

            drawing_squares(context, convert(headx()), convert(heady()), canvas.width / w - 1.5, canvas.height / h - 1.5, 'yellow')



        }

    }

    // converting the values
    function convert(wert) {
        return wert * canvas.height / h;
    }
}


function drawLine(x1, y1, x2, y2, context) {
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
}

// hilf funktionen

// get head of the snake (x)
function headx() {
    return gameState.snake[0].x;
}
// get y of the snake (y)
function heady() {
    return gameState.snake[0].y;

}
// moving the thead according its direction and setting the passed values only to the head of the snake (snkae[0])  

function moving_theHead_to_specific_direction(value1, value2) {

    gameState.snake[0]['x'] += value1;
    gameState.snake[0]['y'] += value2;

}




//checking the posisiton of the seqments of the snake
function segment(gameState, i, j) {
    for (let k = 0; k < gameState.snake.length; k++) {
        if (i == gameState.snake[k].x && j == gameState.snake[k].y) {
            return true;


        }
    }
    return false;

}
// checking the position of the apple
function apple(gameState, i, j) {


    if (i == gameState.apple.x && j == gameState.apple.y) {
        return true;
    }
    return false;

}
// moving just the tail (body)of the snake
function move_body_of_snake(gameState) {
    // from the back to the front
    for (let i = gameState.snake.length - 1; i >= 1; i--) {

        gameState.snake[i]['x'] = gameState.snake[i - 1].x
        gameState.snake[i]['y'] = gameState.snake[i - 1].y

    }

}
// checking the contact with new apple
function check_Contact_with_apple(gameState) {
    if ((gameState.apple.x === headx(gameState)) && (gameState.apple.y === heady(gameState))) {
        return true
    }
    return false
}
function reandom_position_of_food() {

    const randomnumber = Math.floor((Math.random() * gameState.width-1) + 1)
    return randomnumber

}
// if the snake passes the wall then game over  
function gameover(gameState) {


    if (headx() == gameState.width || heady() == gameState.height || headx() <= -1 || heady() <= -1) {
        cancelAnimationFrame(animate);
        return true
    }
    return false
}

// check the collision between the head of the snake and its tail
collision = function (gameState) {

    for (var i = 2; i < gameState.snake.length; i++) {
        if (
            headx() === gameState.snake[i].x &&
            heady() === gameState.snake[i].y
        ) {
            return true;
        }
    }
    return false;
};
// finding a free place for new food 
function find_free_place_for_food() {

    var foodx = reandom_position_of_food()
    var foody = reandom_position_of_food()

    if (match(gameState.snake, foodx, foody)) {

        find_free_place_for_food()

    } else {

        gameState.apple['x'] = foodx
        gameState.apple['y'] = foody
    }

}
// matching between the given position of random food and the tail position of the snake
function match(arr, v1, v2) {
    for (let i = 0; i < arr.length; i++) {
        if (v1 === arr[i]['x'] && v2 === arr[i]['y']) {
            return true
        }
    }
    return false

}

// drawing the squeres 
function drawing_squares(context, x, y, w, h, color) {

    context.fillStyle = color;

    context.fillRect(x, y, w, h);

}





# Snake with JavaScript and canvas.
## to start the game
This snake game has been deploymented with CI/CD and its availabe here: [click here](https://schindar.gitlab.io/-/snake/-/jobs/369256616/artifacts/public/index.html)

## License
[Schindar Ali](https://www.linkedin.com/in/schindar/) 


## Photo of the game

![Snake](/uploads/74a057efe0971bf589d80095d3a632cc/Screenshot_2019-12-04_15-17-59.png)